﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using TestWebThumbnailApp.Helpers;
using TestWebThumbnailApp.Models;

namespace TestWebThumbnailApp.Controllers
{
    [RoutePrefix("api/v1/file")]
    public class FileApiController : ApiController
    {
        [Route("upload")]
        public async Task<HttpResponseMessage> Upload(UploadCropImageVM model)
        {
            if (model == null)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

            try
            {
                var bytes = Convert.FromBase64String(model.original.base64.Substring("data:image/jpeg;base64,".Length));

                using (var memStream = new System.IO.MemoryStream(bytes))
                {
                    FileManager.GenerateThumbs(memStream, model);
                }

                return Request.CreateResponse(HttpStatusCode.OK, new { Status = "OK", Message = "Success" });
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpPost]
        [Route("test")]
        public HttpResponseMessage Test()
        {
            return Request.CreateResponse(HttpStatusCode.OK, "Success");
        }

        
    }
}
