﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TestWebThumbnailApp.Helpers;
using TestWebThumbnailApp.Models;

namespace TestWebThumbnailApp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SelectList()
        {
            return View();
        }

        public JsonResult GetSelectData()
        {
            var data = new List<SelectListItem>();
            data.Add(new SelectListItem() { Text = "Beşiktaş", Value = "1", Selected = true });
            data.Add(new SelectListItem() { Text = "Avcılar", Value = "2" });
            data.Add(new SelectListItem() { Text = "Beykoz", Value = "3" });
            data.Add(new SelectListItem() { Text = "Üsküdar", Value = "4" });
            data.Add(new SelectListItem() { Text = "Kabataş", Value = "5", Selected = true });
            data.Add(new SelectListItem() { Text = "Ortaköy", Value = "6" });
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult ImageGalleryList()
        {
            return PartialView("_ImageGalleryList");
        }

        public JsonResult GetImageData()
        {
            var directory = Server.MapPath("~/Upload");
            var files = FileManager.DirectorySearch(directory);
            for (var i=0;i<files.Count;i++)
            {
                files[i] = files[i].Replace(directory, "\\Upload");
            }
            return Json(files, JsonRequestBehavior.AllowGet);
        }

       
    }
}