﻿function openModal(content, opt) {
    opt = Object.assign({
        autoOpen: false,
        modal: true,
        width: 550,
        height: 650,
        title: 'Details',
        data: [],
        show: { effect: 'clip', duration: 350, times: 3 },
        hide: { effect: 'clip', duration: 350, times: 3 },
        position: ["center", "center"],
        success: function (data) {
            console.log('success', data);
        },
        error: function (error) {
            console.log('error', error);
        }
    }, opt);
    console.log("OPT:", opt);
    var dialogItem = $("<div></div>");
    $('body').append(dialogItem);
    $(dialogItem).html(content);
    $(dialogItem).dialog(opt).dialog('open');
    $(dialogItem).on('dialogclose', function (event) {
        $(this).remove();
    });
    $(dialogItem).on('dialog-success', opt.success);
    $(dialogItem).on('dialog-error', opt.error);
    $(dialogItem).on("click", ".close-dialog", function () {
        $(dialogItem).dialog("close");
    });
    $(dialogItem).data('data', JSON.stringify(opt.data));
}