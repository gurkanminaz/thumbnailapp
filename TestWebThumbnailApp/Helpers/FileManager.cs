﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using TestWebThumbnailApp.Models;

namespace TestWebThumbnailApp.Helpers
{
    public class FileManager
    {
        public static void ResizeImage(int newWidth, int newHeight, Stream fromStream, string filePath)
        {
            var image = Image.FromStream(fromStream);
            var thumbnailBitmap = new Bitmap(newWidth, newHeight);

            var thumbnailGraph = Graphics.FromImage(thumbnailBitmap);
            thumbnailGraph.CompositingQuality = CompositingQuality.HighQuality;
            thumbnailGraph.SmoothingMode = SmoothingMode.HighQuality;
            thumbnailGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;

            var imageRectangle = new Rectangle(0, 0, newWidth, newHeight);
            thumbnailGraph.DrawImage(image, imageRectangle);

            thumbnailBitmap.Save(filePath, image.RawFormat);

            thumbnailGraph.Dispose();
            thumbnailBitmap.Dispose();
            image.Dispose();
        }

        public static void GenerateThumbs(Stream stream, UploadCropImageVM model)
        {
            var root = System.Web.Hosting.HostingEnvironment.MapPath("~/Upload/");
            var image = Image.FromStream(stream);
            var guidImage = RndStr(5);
            var fileName = model.original.filename;
            var newFileName = fileName.Substring(0, fileName.LastIndexOf('.'));
            var fileExt = fileName.Replace(newFileName, "");
            var newFilePath = root + newFileName + "-" + guidImage + fileExt;
            CreateDirectoryIfNotExists(root + "Thumbnail\\");
            // Orginal Image Save
            image.Save(newFilePath);

            // Thumbnails Save
            foreach (var thumbs in GetThumbnailSizes())
            {
                var w = thumbs.Value[0];
                var h = thumbs.Value[1];
                var thumbnailBitmap = new Bitmap(w, h, PixelFormat.Format24bppRgb);
                thumbnailBitmap.SetResolution(80, 60);

                var thumbnailGraph = Graphics.FromImage(thumbnailBitmap);
                thumbnailGraph.CompositingQuality = CompositingQuality.HighQuality;
                thumbnailGraph.SmoothingMode = SmoothingMode.HighQuality;
                thumbnailGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;

                var imageRectangle = new Rectangle(0, 0, w, h);
                thumbnailGraph.DrawImage(image, imageRectangle, model.crop.x, model.crop.y, model.crop.width, model.crop.height, GraphicsUnit.Pixel);
                var thumbPath = root + "Thumbnail/" + newFileName + "-" + guidImage + "-" + thumbs.Key + fileExt;
                thumbnailBitmap.Save(thumbPath, image.RawFormat);

                thumbnailGraph.Dispose();
                thumbnailBitmap.Dispose();
            }

            image.Dispose();
        }

        private static string RndStr(int length)
        {
            Random random = new Random();
            const string chars = "abcdefghijklmnopqrstuvqxyz0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        private static Dictionary<string, int[]> GetThumbnailSizes()
        {
            var list = new Dictionary<string, int[]>();
            list.Add("small", new int[] { 90, 150 });
            list.Add("medium", new int[] { 180, 300 });
            return list;
        }

        public static void CreateDirectoryIfNotExists(string path)
        {
            var fi = new FileInfo(path);
            if (!fi.Directory.Exists)
            {
                fi.Directory.Create();
            }
        }

        public static List<string> DirectorySearch(string dir, List<string> files = null)
        {
            try
            {
                if (files == null) files = new List<string>();

                foreach (string f in Directory.GetFiles(dir))
                {
                    files.Add(Path.GetFullPath(f));
                }
                foreach (string d in Directory.GetDirectories(dir))
                {
                    files = DirectorySearch(d, files);
                }
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return files;
        }
    }
}