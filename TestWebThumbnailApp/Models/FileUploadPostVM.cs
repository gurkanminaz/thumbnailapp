﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestWebThumbnailApp.Models
{
    public class UploadCropImageVM
    {
        public ImageCropVM crop { get; set; }
        public ImageOriginalVM original { get; set; }
    }

    public class ImageCropVM
    {
        public float x { get; set; }
        public float y { get; set; }
        public float width { get; set; }
        public float height { get; set; }
    }

    public class ImageOriginalVM
    {
        public string filename { get; set; }
        public float height { get; set; }
        public float width { get; set; }
        public string base64 { get; set; }
    }
}